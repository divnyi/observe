# observing

Package with different observers.

## Installation

Add to `pubspec.yaml`:

```
dependencies:
  observe:
    git: git@gitlab.com:divnyi/observe.git
```

## Observe

Observe is [ValueNotifier](https://docs.flutter.io/flutter/foundation/ValueNotifier-class.html) 
with some helper methods:

`observe.asyncSet(() async => newValue)` allows to set value of observe using
Futures (and async/await) in non-async methods.

`observe.nextUpdateCompleter()` is useful for unit testing (see examples in `test/`).

`observe.toWidget(...)` is shortcut to building Widget out of Observe object.

## Lazy

`LazyArray<Value>` is, de-facto, just getter, count function and update event
bound together. It should be used in places where you can't store all the 
array in memory (like when you build ListView from very large amount of objects).

use `lazy.map(...)` function to transform values from one type to another, and use
`lazy.toUpdatesListener` and `lazy.toWidgetBuilder` to build the actual ListView:

```
    lazy.toUpdatesListener((context) =>
        ListView.builder(
          itemCount: lazy.length(),
          itemBuilder: lazy.toWidgetBuilder((context, idx, val) =>
              Text(val)),
        )
    );
```

`lazy.asyncAction()` is similar to `observe.asyncSet()`.

`lazy.cache(int length, ...)` will keep in memory last `length` items. Is useful
after complex `.map(...)`.

`lazy.safeguard(...)` and `lazy.safeguardType<Type>(...)` is for error handling.