library observing;

import 'package:flutter/material.dart';
import 'dart:async';

export 'lazy.dart';

class Observe<Type> extends ValueNotifier<Type> {
  Observe(Type value) : super(value);

  Future<void> asyncSet(Future<Type> Function() setter) async {
    this.value = await setter();
  }

  Completer<Type> nextUpdateCompleter() {
    final completer = Completer<Type>();
    final listener = () => completer.complete(this.value);
    this.addListener(listener);
    completer.future.then((val) {
      this.removeListener(listener);
    });
    return completer;
  }

  Widget toWidget(Widget Function(BuildContext context, Type val) builder) {
    return ValueListenableBuilder(
      valueListenable: this,
      builder: (context, val, _) => builder(context, val),
      child: null,
    );
  }
}