import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'observe.dart';
import 'dart:collection';

class Update extends Observe<Null> {
  Update() : super(null);

  void performUpdate() {
    this.notifyListeners();
  }
}

class Lazy<Key, Value, LengthFn> {
  final LengthFn length;
  final Value Function(Key) item;
  Update update;

  Lazy({
    @required this.length,
    @required this.item,
    Update update,
  }) {
    this.update = update ?? Update();
  }

  Value operator [](Key idx) => this.item(idx);

  Lazy<NewKey, NewValue, LengthFn> transform<NewKey, NewValue>({
    Key key(NewKey idx),
    NewValue value(Key idx),
    NewValue newValue(NewKey idx),
    LengthFn newLengthFn,
  }) {
    return Lazy<NewKey, NewValue, LengthFn>(
      length: newLengthFn ?? this.length,
      item: (NewKey idx) {
        var oldIdx = idx as Key;
        if (key != null) {
          oldIdx = key(idx);
        }
        if (oldIdx == null) return newValue(idx);
        if (value != null) {
          return value(oldIdx);
        } else if (NewValue == Value) {
          return this[oldIdx] as NewValue;
        }
      },
      update: this.update,
    )._reType(this);
  }
  Lazy<Key, NewValue, LengthFn> map<NewValue>(NewValue value(Key idx, Value obj)) {
    return this.transform(value: (idx) {
      return value(idx, this[idx]);
    });
  }
  Lazy<Key, Value, LengthFn> asyncAction(Future<void> asyncFn(Key idx, Value obj)) {
    bool isPerformingAsyncAction = false;
    return this.map((key, val) {
      if (isPerformingAsyncAction) return val;
      final asyncAction = asyncFn(key, val);
      if (asyncAction != null) {
        isPerformingAsyncAction = true;
        asyncAction.then((_) {
          this.update.performUpdate();
          isPerformingAsyncAction = false;
        });
      }
      return val;
    });
  }
  Lazy<Key, Value, LengthFn> cache<CacheType>(int length, {
    CacheType toCache (Key key, Value obj),
    Value fromCache (CacheType cache),
  }) {
    if (toCache == null) {
      toCache = (key, obj) => obj as CacheType;
    }
    if (fromCache == null) {
      fromCache = (cache) => cache as Value;
    }
    final cache = Map<Key, CacheType>();
    final cacheMemory = Queue<Key>();

    this.update.addListener(() {
      cache.clear();
      cacheMemory.clear();
    });

    return this.transform(value: (idx) {
      final cached = cache[idx];
      if (cached != null) return fromCache(cached);

      final real = this[idx];
      if (real != null) {
        cacheMemory.add(idx);
        cache[idx] = toCache(idx, real);

        while (cacheMemory.length > length) {
          final key = cacheMemory.removeFirst();
          cache.remove(key);
        }
      }

      return real;
    });
  }
  Lazy<Key, Value, LengthFn> safeguard(Map<Exception, Value Function()> specific) {
    return this.transform(value: (idx) {
      try {
        return this[idx];
      } catch (e) {
        for (var key in specific.keys) {
          if (key == e) {
            return specific[key]();
          }
        }
        rethrow;
      }
    });
  }
  Lazy<Key, Value, LengthFn> safeguardType<ExceptionType>(Value value()) {
    return this.transform(value: (idx) {
      try {
        return this[idx];
      } on ExceptionType catch (_) {
        return value();
      }
    });
  }

  Widget Function(BuildContext context, Key key)
  toWidgetBuilder(Widget Function(BuildContext context, Key key, Value val) builder) {
    final widgetLazy = this.map((key, val) {
      return this.update.toWidget((context, _) {
        return builder(context, key, val);
      });
    });
    return (context, key) => widgetLazy[key];
  }
  Widget toUpdatesListener(Widget Function(BuildContext context) builder, {Widget child}) {
    return this.update.toWidget((context, _) => builder(context));
  }

  dynamic _reType(dynamic self) {
    if (self is LazyArray && LengthFn == Length1d && Key == int) {
      final length = this.length as Length1d;
      final item = this.item as Value Function(int);
      return LazyArray<Value>(length: length, item: item, update: this.update);
    }
    if (self is LazyKeyValue && LengthFn == Length1d) {
      final length = this.length as Length1d;
      return LazyKeyValue<Key, Value>(length: length, item: item, update: this.update);
    }
    if (self is LazyRows && LengthFn == Length2d && Key == IndexPath) {
      final length = this.length as Length2d;
      final item = this.item as Value Function(IndexPath);
      return LazyRows<Value>(length: length, item: item, update: this.update);
    }
    return this;
  }
}

typedef Length1d = int Function();
class Length2d {
  final Length1d columns;
  final int Function(int column) rows;
  Length2d(this.columns, this.rows);
}
class IndexPath {
  final int row;
  final int column;
  IndexPath(this.row, this.column);
}
mixin ToAlias {}
class LazyArray<Value> = Lazy<int, Value, Length1d> with ToAlias;
class LazyRows<Value> = Lazy<IndexPath, Value, Length2d> with ToAlias;
class LazyKeyValue<Key, Value> = Lazy<Key, Value, Length1d> with ToAlias;
