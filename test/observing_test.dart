import 'package:flutter_test/flutter_test.dart';

import 'package:observe/observe.dart';

void main() {
  test('observing', () async {
    final ob = Observe(1);
    expect(ob.value, 1);
    ob.value = 2;
    expect(ob.value, 2);

    var completer = ob.nextUpdateCompleter();
    expect(completer.isCompleted, false);
    ob.asyncSet(() async => 123);
    expect(await completer.future, 123);
    expect(completer.isCompleted, true);
    expect(ob.value, 123);
  });
}
