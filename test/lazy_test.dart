import 'package:flutter_test/flutter_test.dart';

import 'package:observe/observe.dart';
import 'dart:async';

void main() {
  test('simple array lazy', () async {
    final arr = ["asdf", "bcd", "efg"];
    LazyArray<String> lazy = LazyArray<String>(
        length: () {
          return arr.length;
        },
        item: (idx) {
          return arr[idx];
        }
    ).safeguardType<RangeError>(() => null);

    expect(lazy.length(), 3);
    expect(lazy[0], "asdf");
    expect(lazy[1], "bcd");
    expect(lazy[2], "efg");
    expect(lazy[3], null);
    arr[0] = "a";
    expect(lazy[0], "a");
  });
  test('update', () async {
    final arr = ["asdf", "bcd", "efg"];
    final lazy = LazyArray<String>(
        length: () {
          return arr.length;
        },
        item: (idx) {
          return arr[idx];
        }
    ).safeguardType<RangeError>(() => null);

    var completer = new Completer();
    lazy.update.addListener(() => completer.complete(null));
    expect(completer.isCompleted, false);
    lazy.update.notifyListeners();
    expect(completer.isCompleted, true);
  });
  test('async update test', () async {
    final arr = ["asdf", "bcd", "efg"];
    final lazy = LazyArray<String>(
        length: () {
          return arr.length;
        },
        item: (idx) {
          return arr[idx];
        }
    ).safeguardType<RangeError>(() => null)
    .asyncAction((idx, _) {
      if (idx < arr.length) return null;
      return Future.delayed(Duration(milliseconds: 50), () {
        arr.add("new");
      });
    });

    final completer = lazy.update.nextUpdateCompleter();
    expect(lazy[2], "efg");
    expect(completer.isCompleted, false);
    expect(lazy[3], null);
    await completer.future;
    expect(lazy[3], "new");
  });
  test('test cache', () async {
    final arr = ["asdf", "bcd", "efg"];
    final lazy = LazyArray<String>(
        length: () {
          return arr.length;
        },
        item: (idx) {
          return arr[idx];
        }
    ).safeguardType<RangeError>(() => null)
        .cache(2);

    expect(lazy[0], "asdf");
    expect(lazy[1], "bcd");
    expect(lazy[2], "efg");
    expect(lazy[3], null);
    arr[0] = "a";
    arr[1] = "b";
    arr[2] = "c";
    expect(lazy[2], "efg");
    expect(lazy[1], "bcd");
    expect(lazy[0], "a");
    expect(lazy[1], "b");
    expect(lazy[2], "c");

    final complex = lazy.cache<int>(10,
        toCache: (key, val) => key*100,
        fromCache: (cache) => "HZ$cache");
    expect(complex[2], "c");
    expect(complex[2], "HZ200");
  });
}
